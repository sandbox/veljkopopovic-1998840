function ncs_ajax(url, settings) {
  jQuery.ajax(ncs_path(url), settings);
}

function ncs_path (url) {
  var ret = url;
  var s = Drupal.settings.no_cookie_session || {default_drupal_session:true};
  if (!s.default_drupal_session){
    /// very very thin ... should check on this, we need ? and & check ....
    ret+=('?'+s.session_name+'='+s.session_id);
  }
  return ret;
}
